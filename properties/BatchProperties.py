#*******************************************************************************************************
# D e s c r i p t i o n
#*******************************************************************************************************
# Scriptname             : BatchProperties.py
# Current version        : 1.0
# Location               : In properties sub-directory located under batch processing directory. Customer defined. example: (/home/user/testbatch/properties)
# Server                 : any
#
# The script provides properties for running batch scripts.
#*******************************************************************************************************
# C h a n g e   l o g
#*******************************************************************************************************
# Version  Date            Name                Description
# -------  -------         -------             -----------------------------------------------
#     1.0  2023-07-12      BobbyK              initial version


# ----------------------------------- Batch Properties ------------------------------------------------#


class BatchProperties:

     # ----------------------------- Common Properties -----------------------------------------------#
     class BatchEnvironmentProperties:
          
          BATCHDIR='./testbatch'
          BATCHLOGDIR='../logs'
          OUTPUTDIR='../output'
          
          
     class Email:
          Send        = False
          SMTPServer  = ''
          Users       = ['','','']
          
     # ----------------------------- Job Specific Properties -----------------------------------------#
     #
     # Populate with data only relevant to the specific jobs. Jobs are devided in different classes.
     
     class BNB:
          PRODUCTSLIST='./bnb/products.txt'
     
     class PingTest:
          PINGTARGET = 'google.com'
          PINGPACKETS = '10'
          
     class SimpleWebScraper:
          SCRAPEURL = 'https://en.wikipedia.org/wiki/Web_scraping'
