import schedule
import time
import subprocess

# Define the paths to the scripts
script1_path = "./ping_test/ping_test.py"
script2_path = "./alive/alive.py"
script3_path = "./bnb/bnb.py"
script4_path = "./SimpleWebScraper/SimpleWebScraper.py"

def run_script1():
    subprocess.call(["python3", script1_path])

def run_script2():
    subprocess.call(["python3", script2_path])

def run_script3():
    subprocess.call(["python3", script3_path])
    
def run_script4():
    subprocess.call(["python3", script4_path])

# Schedule the execution of script 1 every 1 hour
#schedule.every(1).hours.do(run_script1)

# Schedule the execution of script 2 every 1 hour
#schedule.every(1).hours.do(run_script2)

# Schedule the execution of script 1 every 1 minute
schedule.every(1).minutes.do(run_script1)

# Schedule the execution of script 2 every 1 minute
schedule.every(1).minutes.do(run_script2)

# Schedule the execution of script 3 every 2 minutes
schedule.every(2).minutes.do(run_script3)

# Schedule the execution of script 4 every 2 minutes
schedule.every(2).minutes.do(run_script4)

while True:
    schedule.run_pending()
    time.sleep(15)

