import os
import sys

# Add the parent directory of the current directory to the system path
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)

from properties.BatchProperties import BatchProperties

# Accessing properties using class and attribute syntax
batch_props = BatchProperties()
batch_log_dir = batch_props.BatchEnvironmentProperties.BATCHLOGDIR
batch_output_dir = batch_props.BatchEnvironmentProperties.OUTPUTDIR

# Check if the log directory exists, create it if it doesn't
if not os.path.exists(batch_log_dir):
    os.makedirs(batch_log_dir)
    print(f"Created log directory: {batch_log_dir}")
    
# Check if the output directory exists, create it if it doesn't
if not os.path.exists(batch_output_dir):
    os.makedirs(batch_output_dir)
    print(f"Created output directory: {batch_output_dir}")
