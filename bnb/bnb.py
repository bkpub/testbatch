import csv
import datetime
import os
import sys
import requests
import logging
from bs4 import BeautifulSoup

# Add the parent directory of the current directory to the system path
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)

from properties.BatchProperties import BatchProperties

# Accessing properties using class and attribute syntax
batch_props = BatchProperties()
batch_log_dir = batch_props.BatchEnvironmentProperties.BATCHLOGDIR
batch_output_dir = batch_props.BatchEnvironmentProperties.OUTPUTDIR

# Define the URLs of the websites
bnb_url = "https://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm"

# Define the location of the products file
#products_file = "./bnb/products.txt"
products_file = batch_props.BNB.PRODUCTSLIST
print (products_file)

# Define the location of the output directory and file
output_directory = batch_output_dir
output_file = f"bnb_rate_{datetime.datetime.now():%Y%m%d%H%M%S}.csv"

# Define the location of the log directory and file
log_directory = batch_log_dir
log_file = "bnb.log"

# Check if the log directory exists, create it if it doesn't
if not os.path.exists(log_directory):
    os.makedirs(log_directory)
    print(f"Created log directory: {log_directory}")

# Set up logging
logging.basicConfig(
    filename=os.path.join(log_directory, log_file),
    format="%(asctime)s [%(levelname)s]: %(message)s",
    level=logging.INFO
)

# Check if the output directory exists, create it if it doesn't
if not os.path.exists(output_directory):
    os.makedirs(output_directory)
    logging.info(f"Created output directory: {output_directory}")

# Read the product list from the file
with open(products_file, "r") as file:
    products = file.read().splitlines()

# Send a GET request to the BNB website and retrieve the content
bnb_response = requests.get(bnb_url)
bnb_html_content = bnb_response.text

# Parse the HTML content for BNB using BeautifulSoup
bnb_soup = BeautifulSoup(bnb_html_content, "html.parser")

# Create a list to hold the exchange rates
exchange_rates = []

# Loop through the products and retrieve the exchange rates
for product in products:
    # Find the <td> element with class "center" containing the currency
    currency_td = bnb_soup.find("td", class_="center", string=product)

    if currency_td:
        # Find the next <td> element with class "center" containing the BNB exchange rate
        bnb_rate_td = currency_td.find_next_sibling("td", class_="center")

        # Get the text value of the BNB exchange rate
        bnb_rate = bnb_rate_td.get_text().strip()

        # Convert the BNB exchange rate to a floating-point number
        bnb_rate = float(bnb_rate)

        # Append the currency and rate to the list
        exchange_rates.append((product, bnb_rate))
        logging.info(f"Retrieved exchange rate for {product}: {bnb_rate}")
    else:
        logging.warning(f"Currency {product} not found in the BNB exchange rates.")

# Write the exchange rates to a CSV file
output_path = os.path.join(output_directory, output_file)
with open(output_path, "w", newline="") as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(["Currency", "Rate"])
    writer.writerows(exchange_rates)

logging.info(f"Created output file: {output_path}")
logging.info("Scraping and output generation completed successfully.")

# Log the end of the script
logging.info("----------###----------")
print("BNB scraping done!")

