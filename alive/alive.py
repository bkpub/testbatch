import datetime
import sys
import os

# Add the parent directory of the current directory to the system path
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)

from properties.BatchProperties import BatchProperties

# Accessing properties using class and attribute syntax
batch_props = BatchProperties()
batch_log_dir = batch_props.BatchEnvironmentProperties.BATCHLOGDIR

def write_log_entry():
    current_time = datetime.datetime.now()
    timestamp = current_time.strftime("%Y-%m-%d %H:%M:%S")
    log_entry = f"{timestamp}: I am alive!\n"
    log_file_path = batch_log_dir + '/alive.log'

    with open(log_file_path, "a") as log_file:
        log_file.write(log_entry)

# Call the function to write the log entry
write_log_entry()

print("I am alive!")
