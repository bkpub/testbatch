# Test Batch Utility
---

This Python script executes on a schedule a test batch that is a collection of multiple single batch jobs/operations.

**Disclaimer: This script comes as is with absolutely no guarantee. Use it at your own risk.**

## Requirements

- Python 3.x
- schedule library (`pip3 install schedule`)
- requests library (`pip3 install requests`)
- BeautifulSoup library (`pip3 install beautifulsoup4`)

## Usage

1. Clone the repository or download the script.
2. Install the required libraries by running the following command:
```
pip3 install -r requirements.txt
```
3. Set up the necessary configuration variables inside the ('./properties/BatchProperties.py') file. The properties file holds configurable variables which are applied batch wide as well as job wide.

4. Modify the schedule for each job. Current example is configured in minutes, but can be altered for seconds or hours.

5. Prior first execution of the Test Batch it is advised to execute PrepareEnv.py from the same directory as TestBatch.py. This simple script will reat the properties file and setup the needed log and output directories before the batch execution. It is needed only once.
```
python3 PrepareEnv.py
```
6. Run the script by executing the following command:
```
python3 TestBatch.py
```
7. Once the script is executed, it will call each job per it's schedule as well as reset iself in 15sec upon execution completion.

## Output

No output is provided by the Test Batch script itself, but any print output present in the jobs will be writen as output to the terminal.

## Logging

No logging is provided for the Test Batch script as logging is intended to be performed from each job individually with it's specific log level and details.

## Batch jobs collection

This repository holds a collection on a few simple batch jobs

- alive - Offline batch job just writing a line to a file on regular basis.
- ping_test - Online batch job pinging desired website and writing the result to a log file.
- SimpleWebScraper - As the name implies, this is a simple web scraper downloading the headers from a website and creating an output with the list.
- bnb - Another web scraper, this time more complex, to read exchange rate of different currencies to BGN.

**Disclaimer: Please use web scraping and ping utilities with ethics and ensure that you comply with the terms of service and policies of the websites you use.**

## License

This project is licensed under the [MIT License](LICENSE).

### Notes

- Any new jobs can be added to this scripts but they have to be configured individually in the script.
- If uncertain which requirements are needed you can run the steps below to create a requirements.txt list by scanning the python scripting int he testbatch directory.
```
pip3 install pipreqs
pipreqs .
```


