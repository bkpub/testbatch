import os
import sys
import requests
from bs4 import BeautifulSoup
from datetime import datetime
import logging

# Add the parent directory of the current directory to the system path
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)

from properties.BatchProperties import BatchProperties

# Accessing properties using class and attribute syntax
batch_props = BatchProperties()
batch_log_dir = batch_props.BatchEnvironmentProperties.BATCHLOGDIR
batch_output_dir = batch_props.BatchEnvironmentProperties.OUTPUTDIR
scrape_url = batch_props.SimpleWebScraper.SCRAPEURL

# URL to scrape.
# Setup manually or use properties file.
url = scrape_url

# Output directory
# Setup manually or use properties file.
output_directory = batch_output_dir

# Define the location of the log directory and file
log_directory = batch_log_dir
log_file = "SimpleWebScraper.log"

# Set up logging
logging.basicConfig(
    filename=os.path.join(log_directory, log_file),
    format="%(asctime)s [%(levelname)s]: %(message)s",
    level=logging.INFO
)

# Create a timestamp for the output file name
timestamp = datetime.now().strftime("%Y%m%d%H%M%S")

# Output file path with timestamp
output_file = "Web_Scrape_{}.txt".format(timestamp)
output_path = os.path.join(output_directory, output_file)

# Send a GET request to the URL
response = requests.get(url)

if response.status_code == 200:
    # Parse the HTML content using BeautifulSoup
    soup = BeautifulSoup(response.content, "html.parser")

    # Find all the headers in the page
    headers = soup.find_all("span", class_="mw-headline")

    # Write the headers to the output file with timestamp
    with open(output_path, "w") as file:
        file.write("Timestamp: {}\n\n".format(timestamp))
        for header in headers:
            file.write(header.text + "\n")

    logging.info(f"Created output file: {output_path}")
    logging.info("Scraping and output generation completed successfully.")

    print("Simple Web Scraper completed successfully!")
else:
    logging.error(f"Failed to retrieve data from URL: {url}")
    logging.error(f"Response status code: {response.status_code}")

    print("Simple Web Scraper error:")
    print(f"Failed to retrieve data from URL: {url}")
    print(f"Response status code: {response.status_code}")

# Log line separator for the end of the script
logging.info("----------###----------")

