# Simple Web Scraping Script

This Python script performs web scraping on a specified URL to extract headers from the webpage. The scraped headers are then saved to a text file along with a timestamp.

**Disclaimer: This script comes as is with absolutely no guarantee. Use it at your own risk.**

## Requirements

- Python 3.x
- requests library (`pip3 install requests`)
- BeautifulSoup library (`pip3 install beautifulsoup4`)

## Usage

1. Clone the repository or download the script.
2. Install the required libraries by running the following command:
```
pip3 install -r requirements.txt
```
3. Set up the necessary configuration variables:
- URL to scrape: Modify the `url` variable in the script to specify the URL you want to scrape.
- Output directory: Modify the `output_directory` variable in the script to specify the directory where the output file will be saved.
- This version of the script uses an external properties file to load these variables.
4. Run the script by executing the following command:
```
python3 SimpleWebScraper.py
```
5. Once the script is executed, it will perform web scraping on the specified URL, extract the headers, and save them to a text file with a timestamp in the specified output directory.
6. The script will also log the execution details in a log file named `SimpleWebScraper.log` located in the `./log` directory.

## Output

The script will create a text file in the specified output directory. The output file will have a name in the following format: Web_Scrape_YYYYMMDDHHMMSS.txt


The file will contain the scraped headers, each on a separate line, along with a timestamp indicating when the scraping was performed.

## Logging

The script logs its execution details to a log file named `SimpleWebScraper.log` located in the `./log` directory. The log file includes information such as the timestamp, log level, and log message.

## License

This project is licensed under the [MIT License](LICENSE).

## Notes

- This script uses the requests library to send a GET request to the specified URL and the BeautifulSoup library to parse the HTML content.
- Ensure that you have the necessary permissions to write files in the specified output directory.
- Please be mindful of web scraping ethics and ensure that you comply with the terms of service and policies of the websites you scrape.


