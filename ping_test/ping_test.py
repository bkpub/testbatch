#!/usr/bin/env python3

##########################################################
# Script Name: ping_test.py
# Creator: Borislav Kostov
# Description: This script pings google.com with 10 packets and logs the results.
# Usage: Used to monitor connection disruptions.
#
# Revision History:
# Revision      Date        Author          Description
#---------------------------------------------------------
# 1.0      2023-06-19  BobbyK        Initial version
# 2.0      2023-06-20  BobbyK        Added retry functionality
# 3.0      2023-06-28  BobbyK        Moved log file to separate directory and added date tagging to the name of the log file.
# 4.0      2023-07-16  BobbyK       Read log location from properties file.
# 5.0      2023-07-17  BobbyK        Wrapped inside a 'main' function. It will execute once and then exit, without repeating the execution.
#
##########################################################

import os
import sys
import subprocess
from datetime import datetime

# Add the parent directory of the current directory to the system path
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
sys.path.append(parent_dir)

from properties.BatchProperties import BatchProperties

def main():
	# Accessing properties using class and attribute syntax
	batch_props = BatchProperties()
	batch_log_dir = batch_props.BatchEnvironmentProperties.BATCHLOGDIR

	logs_dir = batch_log_dir
	log_file = os.path.join(logs_dir, f"ping_log_{datetime.now().strftime('%Y-%m-%d')}.log")
	# Use these variable to manually set.
	#target = "google.com"
	#packets = 10
	# These variables use properties from and espternal properties file. Always comment out one or the otper option, but not simultanious.
	target = batch_props.PingTest.PINGTARGET
	packets = batch_props.PingTest.PINGPACKETS

	print(f"Pinging {target}...")
	print(f"Logging results to {log_file}")

	# Check if the logs directory exists, create it if not
	if not os.path.isdir(logs_dir):
		os.mkdir(logs_dir)

	# Function to log results
	def log_result(result):
		timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		with open(log_file, "a") as file:
			file.write(f"{timestamp}: {result}\n")

	# Ping the target and log results
	ping_result = subprocess.run(["ping", "-c", str(packets), target], capture_output=True, text=True)
	ping_exit_code = ping_result.returncode

	if ping_exit_code == 0:
		log_result(ping_result.stdout)
		print(f"Ping completed successfully. Results logged to {log_file}")
	else:
		log_result(f"Error: Unable to ping {target}")
		print(f"Error: Unable to ping {target}. Check your network connectivity or DNS settings.")

	with open(log_file, "a") as file:
		file.write("----------------------------------------\n")  # Append a line of dashes to the log file

if __name__ == "__main__":
    main()

